import styled from 'styled-components';

const FieldBaseWrapper = styled.div`
  flex: 1 1 auto;
`;

FieldBaseWrapper.displayName = 'FieldBaseWrapper';

export default FieldBaseWrapper;
