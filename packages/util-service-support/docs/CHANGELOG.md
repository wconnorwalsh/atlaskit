# @atlaskit/util-service-support

## 2.0.2 (2017-09-12)

* bug fix; requestService can handle 204 responses with no content ([edf13d5](https://bitbucket.org/atlassian/atlaskit/commits/edf13d5))





## 2.0.1 (2017-07-24)


* fix; make sure types from utils are exports (extracted types to separate file) ([ebde291](https://bitbucket.org/atlassian/atlaskit/commits/ebde291))

## 1.0.0 (2017-07-24)


* feature; extract common service integration code into a shared library ([5714832](https://bitbucket.org/atlassian/atlaskit/commits/5714832))
