import styled from 'styled-components';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

export default styled.div`
  display: flex;
  justify-content: center;
  padding-right: ${akGridSizeUnitless}px;
`;
