# Analytics

Fire analytics events from React Components and then decorate and listen for them.

## Installation

```sh
npm install @NAME@
```

## Usage

Detailed docs and example usage can be found [here](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).
