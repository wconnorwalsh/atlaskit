# @atlaskit/analytics

## 2.1.0 (2017-10-05)

* feature; action/decision related analytics (issues closed: fs-1290) ([38ade4e](https://bitbucket.org/atlassian/atlaskit/commits/38ade4e))


## 2.0.0 (2017-09-13)

* breaking; AnalyticsListener and AnalyticsDecorator now only accepts one child and will throw an error if ([92f1b3f](https://bitbucket.org/atlassian/atlaskit/commits/92f1b3f))
* breaking; analyticsListener and AnalyticsDecorator now only accepts one child (issues closed: ak-2048) ([92f1b3f](https://bitbucket.org/atlassian/atlaskit/commits/92f1b3f))
## 1.1.2 (2017-09-05)


* bug fix; expose innerRef on WithAnalytics ([3f8a210](https://bitbucket.org/atlassian/atlaskit/commits/3f8a210))
## 1.1.1 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))



## 1.1.0 (2017-08-03)



* feature; allow analytics components to set a default analyticsId and analyticsData (issues closed: ak-3162) ([6c5ce68](https://bitbucket.org/atlassian/atlaskit/commits/6c5ce68))



## 1.0.3 (2017-07-31)

* bug fix; fixed analytics partial string match (issues closed: ak-3072) ([328a204](https://bitbucket.org/atlassian/atlaskit/commits/328a204))



## 1.0.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 1.0.0 (2017-07-18)


* fix; correct entrypoint for ak:webpack:raw ([f76254f](https://bitbucket.org/atlassian/atlaskit/commits/f76254f))
* fix; remove anayticsDelay feature from initial scope ([dcd471c](https://bitbucket.org/atlassian/atlaskit/commits/dcd471c))


* feature; add analytics package ([19fda60](https://bitbucket.org/atlassian/atlaskit/commits/19fda60))
