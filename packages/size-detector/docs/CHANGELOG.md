# @atlaskit/size-detector

## 2.0.1 (2017-10-03)

* bug fix; Fix incorrect propType declaration ([46d1f50](https://bitbucket.org/atlassian/atlaskit/commits/46d1f50))
