# @atlaskit/toggle

## 2.6.1 (2017-09-28)

* bug fix; active state is now properly applied ([c1ee840](https://bitbucket.org/atlassian/atlaskit/commits/c1ee840))
* bug fix; use withTheme to get theme for icon color ([1b554a1](https://bitbucket.org/atlassian/atlaskit/commits/1b554a1))
* bug fix; update toggle darkmode colors ([e2e4727](https://bitbucket.org/atlassian/atlaskit/commits/e2e4727))
## 2.6.0 (2017-09-18)

* feature; add dark mode to toggle ([ec57939](https://bitbucket.org/atlassian/atlaskit/commits/ec57939))
## 2.5.0 (2017-09-13)

* feature; update dependencies for toggle ([ae1a0ca](https://bitbucket.org/atlassian/atlaskit/commits/ae1a0ca))
## 2.4.3 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))








## 2.4.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 2.4.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 2.1.0 (2017-07-17)

## 2.1.0 (2017-07-17)

## 2.1.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 2.1.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 2.0.5 (2017-07-13)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 2.0.4 (2017-05-18)


* fix; remove state from ToggleStateless ([631252c](https://bitbucket.org/atlassian/atlaskit/commits/631252c))

## 2.0.3 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 2.0.2 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 2.0.1 (2017-04-11)


* fix; update toggle readme story with new readme component ([31f7e4a](https://bitbucket.org/atlassian/atlaskit/commits/31f7e4a))

## 2.0.0 (2017-04-03)


* fix; refactor the toggle component to use styled-components ([c735164](https://bitbucket.org/atlassian/atlaskit/commits/c735164))


* breaking; added peerDependency "styled-components”, removed dependency “classnames”

ISSUES CLOSED: AK-2004

## 1.0.6 (2017-03-23)


* fix; Empty commit to release the component ([49c08ee](https://bitbucket.org/atlassian/atlaskit/commits/49c08ee))

## 1.0.4 (2017-03-21)

## 1.0.4 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.3 (2017-02-16)


* fix; refactor stories to use // rather than http:// ([a0826cf](https://bitbucket.org/atlassian/atlaskit/commits/a0826cf))

## 1.0.2 (2017-02-09)


* fix; avoiding binding render to this ([40c9951](https://bitbucket.org/atlassian/atlaskit/commits/40c9951))

## 1.0.1 (2017-02-07)


* fix; Updates package to use scoped ak packages ([4923aa0](https://bitbucket.org/atlassian/atlaskit/commits/4923aa0))
