import productStatusChecker from '../common/productStatusChecker';

export default productStatusChecker('jira-servicedesk.ondemand');
