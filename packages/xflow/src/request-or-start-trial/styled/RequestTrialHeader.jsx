import styled from 'styled-components';

const RequestTrialHeader = styled.div`
  display: inline-flex;
  align-items: center;
  flex-wrap: wrap;
`;

RequestTrialHeader.displayName = 'RequestTrialHeader';
export default RequestTrialHeader;
