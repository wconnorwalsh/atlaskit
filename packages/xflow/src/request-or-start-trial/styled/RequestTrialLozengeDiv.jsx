import styled from 'styled-components';

import { gridSize, math } from '@atlaskit/theme';

const RequestTrialLozengeDiv = styled.div`
  margin-left: ${math.multiply(gridSize, 1.5)}px;
`;

RequestTrialLozengeDiv.displayName = 'RequestTrialLozengeDiv';
export default RequestTrialLozengeDiv;
