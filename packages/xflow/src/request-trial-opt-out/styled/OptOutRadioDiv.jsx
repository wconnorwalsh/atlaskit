import styled from 'styled-components';

import { gridSize, math } from '@atlaskit/theme';

const OptOutRadioDiv = styled.div`
  margin-top: -${math.multiply(gridSize, 1.5)}px;
`;

OptOutRadioDiv.displayName = 'OptOutRadioDiv';
export default OptOutRadioDiv;
