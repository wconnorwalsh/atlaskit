# Theme

Theme has a component export and several other helper exports. Together they allow
easy theming within an AtlasKit-using project.

## Try it out

Interact with a [live demo of the @NAME@ component with code examples](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Installation

```sh
npm install @NAME@
```
